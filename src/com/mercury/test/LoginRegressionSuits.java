package com.mercury.test;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LoginRegressionSuits {

	WebDriver driver;
	MethodRepository rep= new MethodRepository();
	
	@BeforeMethod
	public void Applaunch() throws InterruptedException
	{
		rep.browserApplicationLaunch();
	}
	
	/*
	 * TC_001: Verifying valid login functionality
	 */
	@Test(priority=0,enabled=true,description="TC_001: Verifying valid login functionality")
	public void verifyValidLogin()
	{
		try {
			rep.login("dasd", "dasd");
			Assert.assertEquals(true, rep.verifyValidLogin());
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	
	/*
	 * TC_002: Verifying Invalid login functionality
	 */
	@Test(priority=1,enabled=true,description="TC_001: Verifying Invalid login functionality")
	public void verifyInValidLogin()
	{
		try {
			rep.login("das1","das2");
			Assert.assertEquals(true, rep.verifyInValidLogin());
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	
	@AfterMethod
	public void appClose()
	{
		rep.appClose();
	}
}
