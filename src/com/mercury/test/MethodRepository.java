package com.mercury.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class MethodRepository {
	
	//njnjnjnjnj
	
	WebDriver driver;
	public void browserApplicationLaunch() throws InterruptedException
	{
		//system= class; setProperty= method
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://newtours.demoaut.com/");
		Thread.sleep(3000);
		
		
	}
	
	public void login(String uname1, String pwd) throws InterruptedException
	{
		WebElement uname= driver.findElement(By.xpath("//input[@name='userName']"));
		uname.sendKeys(uname1);
		
		WebElement pswd= driver.findElement(By.xpath("//input[@name='password']"));
		pswd.sendKeys(pwd);

        WebElement submit= driver.findElement(By.xpath("//input[@name='login']"));
        submit.click();
		
		Thread.sleep(5000);
	}
	
	public boolean verifyValidLogin()
	{
		String expTitle= "Find a Flight: Mercury Tours:";
		String actTitle= driver.getTitle();
		if(expTitle.equals(actTitle))
		{
			return true;
		}
		else 
		{
			return false;
			
		}
	}
	
	public boolean verifyInValidLogin()
	{
		String expTitle= "Find a Flight: Mercury Tours:";
		String actTitle= driver.getTitle();
		
		System.out.println(actTitle +" "+ expTitle);
		
		if(expTitle!=(actTitle))
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	public boolean verifyDefaultSelectionRoundTrip()
	{
		WebElement radioBtnOneWay= driver.findElement(By.xpath("//input[@value='oneway']"));
		WebElement radioBtnRoundTrip= driver.findElement(By.xpath("//input[@value='roundtrip']"));
		
		if(radioBtnOneWay.isSelected()==true && radioBtnRoundTrip.isSelected()==false)
		{
			return true;
		}
		
		else
		{
			return false;
		}
	}
	
	public boolean departingFromValueSelection()
	{
		WebElement dropdownDepertingFrom= driver.findElement(By.xpath("//select[@name='fromPort']"));
		Select s1= new Select(dropdownDepertingFrom);
		s1.selectByVisibleText("London");
		
		if(dropdownDepertingFrom.getText().equals("London"))
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	public void appClose()
	{
		driver.quit();
	}

}
