package com.mercury.test;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class FlightFinderRegressionSuits {

	//ABC_NEW
	WebDriver driver;
	MethodRepository rep= new MethodRepository();
	
	@BeforeMethod
	public void Applaunch() throws InterruptedException
	{
		rep.browserApplicationLaunch();
	}
	
	/*
	 * TC_001: Verifying default selection of Flight type
	 */
	@Test(priority=0,enabled=true,description="TC_001: Verifying default selection of Flight type")
	public void verifyDefaultSelectionFlightType()
	{
		try {
			rep.login("dasd","dasd");
			Assert.assertEquals(true, rep.verifyValidLogin());
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	
	/*
	 * TC_002: Verifying departing From Value Selection
	 */
	@Test(priority=0,enabled=true,description="TC_001: Verifying default selection of Flight type")
	public void verifyDepartingFromValueSelection()
	{
		try {
			rep.login("dasd", "dasd");
			Assert.assertEquals(true, rep.departingFromValueSelection());
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	
	@AfterMethod
	public void appClose()
	{
		rep.appClose();
	}
}
